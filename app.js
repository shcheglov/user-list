<app>
	<app-header change="{changeType}" active="{state}" search="{search}"></app-header>
	<h4 show="{nofound}">{nofound}</h4>
  <user-table if="{ state == 'table' }" sorted="{sorted}" data="{data}" search="{searchText}"></user-table>
  <user-card   if="{ state == 'card' }" sorted="{sorted}" data="{data}" search="{searchText}"></user-card>
  <user-group if="{ state == 'group' }" sorted="{sorted}" data="{data}" search="{searchText}"></user-group>
	<script>
		this.data = [];
		this.staticData = [];
    this.sorted = "id"
    this.state = 'table';
		this.searchText= '';
		this.search = (e) => {
			  this.searchText = (e.target.value);
        searchByName(e.target.value);

		};
    this.changeType = (e) => {
      this.update({state: e.target.name});
      console.log(this.state);
    };
		this.on('mount', () => {
                if (window.localStorage.getItem('user-lsit__data')) {
                    var data = JSON.parse(window.localStorage.getItem('user-lsit__data'));
                    this.update({data: data, staticData: data, nofound: ''});
                } else {
                   fetch('http://www.json-generator.com/api/json/get/bQtMNeBWJK?indent=2').then((res) => {
                        return res.json();
                    }).then((data) => {
                        window.localStorage.setItem('user-lsit__data', JSON.stringify(data));
                        this.update({data: data, staticData: data, nofound: ''});
                    }) 
                    }
      		});


		// поиск с debounce
    var searchByName = _.debounce(function(search, data) {
                console.time('search');
                    var search = search.toLowerCase();
                    var d = _.filter(this.staticData, function(o) {
                         return o.name.toLowerCase().indexOf(search.toLowerCase()) !== -1; 
                     });
                    d = _.orderBy(d, this.sorted, this.order ? 'asc' : 'desc');

                    this.update({data: d, nofound: d.length ? '': 'Никто не найден по вашему запросу'});
                console.timeEnd('search');     
            }, 500).bind(this);


	</script>
  <style>
    h4 {
    text-align: center;
    background: #000;
    margin: 0;
    color: #fff;
    padding: 10px;
    }
  </style>
</app>