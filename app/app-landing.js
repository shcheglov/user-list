<app-landing>
<div class="u_landing">
  <h1>Задание №2</h1>
  <div class="u_desc">
  <ul>
  	<li>фреймворк: riot.js</li>
  	<li>Приложение имеет 2 роута: <ul>
  		<li>этот (на котором вы сейчас) | <a href="#/list">и этот</a></li>
  	</ul></li>
  	<li>Данные из json-generator (http://www.json-generator.com/api/json/get/bQtMNeBWJK?indent=2) потом храню в localstorage</li>
  	<li>Производительность все основные математические операции прологированны в консоли<img src="/img/temp/console.png"/>
  	<div>
  	Я считаю: все что меньше 50ms - супер, 50-150ms приемлемо (Приложение входит в мои рамки);	
  	</div>
  	</li>
  	<li>!!! В коде использовались возможности ES6 такие как: fetch и стрелочные функции, данная версия не подходит для публикации, и создавалась только для превью версии. Просьба использовать последние версии браузеров</li>
  	<li>Сотрировка в табличном виде осуществляется кликом по заголовоку столбца</li>
  	<li><a href="https://bitbucket.org/shcheglov/user-list">Исходник кода</a></li>
  </ul>

  </div>
  <a class="large button expanded" href="#/list">Перейти к результатам</a>
</div>
<style>
	.u_landing {
		text-align: center;
		text-align: center;
    	margin: 80px;
	}
	.u_desc {
		text-align: left;
	}
</style>
</app-landing>