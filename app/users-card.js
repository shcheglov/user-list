<user-card>
<form>
  <div class="row sorted">
    <div class="small-3 columns">
      <label class="text-right">Сортировка:</label>
    </div>
    <div class="small-9 columns">
	  <select onchange="{selectChange}">
	    <option value="{['id', 'asc']}">id &uarr;</option>
	    <option value="{['id', 'desc']}">id &darr;</option>
	    <option value="{['name', 'asc']}">name &uarr;</option>
	    <option value="{['name', 'desc']}">name &darr;</option>
	    <option value="{['department', 'asc']}">department &uarr;</option>
	    <option value="{['department', 'desc']}">department &darr;</option>
	  </select>
    </div>
  </div>
</form>
	<div class="u_cards">
		<div class="u_card" each="{data}">
		    
		    <div class="u_card__id">id: {id}</div>
			<div class="u_card__photo"><img src="img/{gender}.png"/></div>
			<div class="u_card__name">{name}</div>
			<div class="u_card__department">{department}</div>
			<div class="u_card__email">{phone}</div>
		</div>
	</div>

	<script>
		this.data = opts.data;
	    this.sorted = opts.sorted || 'id';
	    this.order = opts.order || 'asc';
		selectChange(e) {
			var item = e.target.value.split(',');
			this.sorted = item[0];
			this.order = item[1];
			this.sort(this.sorted, this.order)
		}
		sort(name, order) {
               console.time('order');
                    var mew =  _.orderBy(this.data, name, order);
                    this.data = mew; 
               console.timeEnd('order');
               return true;
        }
		
	</script>
	<style scoped>
	form {
		background: #f8f8f8;
		background: #4967bb;
		 margin: 0 auto 20px;
	}
	form label {
		font-size: 16px;
    	text-align: center;
    	color: #fff;
    	line-height: 39px;
	}
	select {
		margin: 0;
		font-weight: bold;
		text-transform: uppercase;
	}
		.u_cards {
	    position: relative;
	}
	.u_card {
	    width: 222px;
	    display: inline-block;
	    margin: 10px 20px;
	    background: #e6e6e6;
	    vertical-align: top;
	    box-sizing: border-box;
	    padding: 20px;
	    min-height: 272px;
	    overflow: hidden;
	    border-radius: 5px;
	    text-align:center;
	    position: relative;
	}
	.u_card__photo img {
		opacity: .5;
	}
	.u_card__photo {
		margin-bottom: 10px;
	}
	.u_card__name {
		font-weight:bold;
	}
	.u_card__email {
		font-size: 10px;
	}
	.u_card__age {
    	vertical-align: top;
    	position: absolute;
    	right: 0;
    	top: 0px;
    	font-weight: normal;
    	padding: 10px;
	}
	.u_card__id {
    	vertical-align: top;
    	position: absolute;
    	font-size: 10px;
    	left: 0;
    	top: 0px;
    	font-weight: normal;
    	padding: 10px;
	}
	</style>
</user-card>