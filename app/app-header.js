<app-header>
    <div class="top-bar">
      <div class="top-bar-left">
        <ul class="menu ">
          <li class="menu-text"><a href="#/">USER LIST</a></li>
          <li><a class="{active : opts.active == 'table'} "name="table" onclick="{opts.change}" href="#/list">Таблица</a></li>
          <li><a class="{active : opts.active == 'card'} "name="card" onclick="{opts.change}" href="#/list">Карточки</a></li>
          <li><a class="{active : opts.active == 'group'} "name="group" onclick="{opts.change}" href="#/list">Группы</a></li>
          <li class="timers" if={opts.static}><span>{opts.static}</span></li>
        </ul>
      </div>
      <div class="top-bar-right">
        <ul class="menu">
          <li><input type="search" name="s" oninput={opts.search} onchange={opts.search} placeholder="Search"></li>
        </ul>
      </div>
    </div>
  
  <script>

  </script>

  <style scoped>
    .top-bar input {
      margin-right: 0;
    }
    .menu a.active {
      background: #4967bb;
      color: #fff;
      /*font-weight: bold;*/
    }
    .menu-text a {
      padding: 0;
      margin: 0;
      color:#000;
    }
  </style>

</app-header>