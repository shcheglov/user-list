<!-- Трелло стайл -->
<card>
	<script>
	this.on('update', () => {
		var data = [];
		for (var i = 0; i < opts.data.length; i++) {
			data.push('<div class="o_card"><div class="o_card--name">' + opts.data[i].name + '</div><div class="o_card--age">' + opts.data[i].id + '</div><div class="o_card--phone">' + opts.data[i].phone + '</div><div class="o_card--email">' + opts.data[i].email + '</div></div>');
		}
		data = data.join('')
		this.root.innerHTML = data;
	});
	</script>
	<style>
		card {
	        max-height: 1000px;
		    overflow-y: auto;
		    display: block;
		}
	</style>
</card>
<user-group>
<form>
  <div class="row sorted">
    <div class="small-3 columns">
      <label class="text-right">Сортировка:</label>
    </div>
    <div class="small-9 columns">
	  <select onchange="{selectChange}">
	    <option value="{['id', 'asc']}">id &uarr;</option>
	    <option value="{['id', 'desc']}">id &darr;</option>
	    <option value="{['name', 'asc']}">name &uarr;</option>
	    <option value="{['name', 'desc']}">name &darr;</option>
	    <option value="{['age', 'asc']}">age &uarr;</option>
	    <option value="{['age', 'desc']}">age &darr;</option>
	  </select>
    </div>
  </div>
</form>
<div class="o_wrap">
	<div class="o_lists">
		<section class="list" each="{ name, i in data}">
			<header><h2>{name ? name : 'not department'}</h2><span>{i.length}</span></header>
			<card name="{name}" data="{i}"></div>
		</section>
	</div>
</div>
<script>
	this.data = opts.data;
	this.staticData = opts.data;
	this.search = opts.search;
	this.sorted = opts.sorted || 'id';
	this.order = opts.order || 'asc';
	selectChange(e) {
		var item = e.target.value.split(',');
		this.sorted = item[0];
		this.order = item[1];
		this.sort(this.sorted, this.order)
	}
	sort(name, order) {
		var result = _.forEach(this.data, (value, key) => {
		  this.data[key] = _.orderBy(value, name, order);
		});
		this.update({data: result});
    }
	this.on('mount', () => {
		this.structured(this.data);
	});
	this.on('update', () => {
		if(this.staticData !== opts.data) {
			this.structured(opts.data);
		}
	});
	structured(data) {
		console.time('mount');
		var result = _.groupBy(data, "department");
		this.update({data: result, staticData: data, search: opts.search});
		console.timeEnd('mount');
	};
</script>
<style scoped>
.o_card--email {
	font-size: 12px;
}
.o_wrap {
	width: 1050px;
	overflow:hidden;
	display: block;
	overflow-x:visible;
	background-color: #4967bb;
}
.o_card--phone {
	font-size: 14px;
}
.o_card--name {
	font-weight: bold;
}
.o_card--age {
    font-weight: normal;
    position: absolute;
    top: 0;
    right: 0;
    margin: 10px;
}
.o_card {
    background-color: #fff;
    border-bottom: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    margin-bottom: 6px;
    box-sizing: border-box;
    max-width: 300px;
    min-height: 20px;
    padding: 10px;
    margin-left: 10px;
    margin-right: 10px;
    z-index: 0;
    position: relative;
}

header {
    position: relative;
    padding: 10px;
    font-weight: 700
}

header h2 {
    text-transform: uppercase;
    font-size: 18px;
    letter-spacing: 1px;
    margin-bottom: 0;
}

header span {
    height: 20px;
    width: 20px;
    position: absolute;
    line-height: 20px;
    top: 0;
    right: 0;
    background: #000;
    border-radius: 30px;
    color: #fff;
    margin: 10px;
    text-align: center;
    font-size: 10px
}

.o_lists {
    background-color: #4967bb;
    display: table;
    border-collapse:separate;
    border-spacing:10px;
}

.list {
    background-color: #e3e3e3;
    border-radius: 3px;
    margin: 5px 5px;
    padding: 0;
    width: 260px;
    display: table-cell;
    vertical-align: top;
}

.list>header {
    font-weight: 700;
    font-size: 15px;
    line-height: 18px;
    cursor: grab
}

header {
    font-weight: 700
}

form {
    background: #4967bb;
    margin: 0 auto 0
}

form label {
    font-size: 16px;
    text-align: center;
    color: #fff;
    line-height: 39px
}

select {
    margin: 0;
    font-weight: 700;
    text-transform: uppercase
}
</style>
</user-group>