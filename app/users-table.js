<user-table>
	<table class="t__table stack hover">
    <thead>
        <tr onclick="{sort}">
            <th class="{active: sorted == 'id', t__id: 1, order: order}">ID</th>
            <th class="{active: sorted == 'name', t__name: 1, order: order}">Name</th>
            <th class="{active: sorted == 'age', t__age: 1, order: order}">Age</th>
            <th class="{active: sorted == 'email', t__email: 1, order: order}">Email</th>
            <th class="{active: sorted == 'department', t__department: 1, order: order}">Department</th>
        </tr>
    </thead>

    <tbody>
        <tr each={data}>
            <td>{id}</td>
            <td>{name}</td>
            <td>{age}</td>
            <td>{email}</td>
            <td>{department}</td>
        </tr>
           </tbody>
	</table>




		<script>
			      this.data = opts.data;
            this.staticData = opts.data;
            this.nofound = 'Загружаем...';
            this.order = true;
            this.sorted = opts.sorted;
            this.static = '';
			      var self = this;

            // Toggle сортировка
            sort(e) {
               this.order = this.sorted == e.target.innerText.toLowerCase() ? !this.order : true; 
               console.time('order');
                    this.sorted = e.target.innerText.toLowerCase();
                    var mew =  _.orderBy(this.data, this.sorted, this.order ? 'asc' : 'desc');
                    this.data = mew; 
               console.timeEnd('order');
               return true
            }
		</script>
    <style scoped>
      th {
        cursor: pointer;
        border-top: 3px solid #f8f8f8;
        border-bottom: 3px solid #f8f8f8;
      }
      th.active {
          background: #e6e6e6;
          border-top: 3px solid #e6e6e6;
          border-bottom: 3px solid #b91414;
      }
      th.active.order {
          border-bottom: 3px solid #e6e6e6;
          border-top: 3px solid #299131;
      }
    </style>
</user-table>